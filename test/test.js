#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global it */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const USERNAME = 'admin';
    let PASSWORD = 'changeme123';

    let browser, app;

    before(function () {
        browser = new Builder().forBrowser('chrome').setChromeOptions(new Options().windowSize({ width: 1280, height: 1024 })).build();
    });

    after(function () {
        browser.quit();
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function login() {
        await browser.get(`https://${app.fqdn}/authentication/login/`);

        await waitForElement(By.id('id_username'));

        PASSWORD = await browser.findElement(By.xpath('//div[@class="content login"]/p[3]/strong[3]')).getText();
        console.log('Got generated password', PASSWORD);

        await browser.findElement(By.id('id_username')).sendKeys(USERNAME);
        await browser.findElement(By.id('id_password')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//form')).submit();

        await waitForElement(By.xpath('//h3[@title="Dashboard"]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/#/home/`);

        await waitForElement(By.xpath('//a[@class="dropdown-toggle" and contains(text(), "User")]'));
        await browser.findElement(By.xpath('//a[@class="dropdown-toggle" and contains(text(), "User")]')).click();

        await waitForElement(By.xpath('//a[@href="/authentication/logout/"]'));
        await browser.findElement(By.xpath('//a[@href="/authentication/logout/"]')).click();

        await waitForElement(By.id('id_username'));
    }

    async function uploadDocument() {
        // start directly with default document type to avoid clicking the select
        await browser.get(`https://${app.fqdn}/#/sources/documents/upload/new/interactive/?document_type_id=1`);

        await waitForElement(By.xpath('//span[contains(text(), " Drop files or click here to upload files")]'));

        // make input element visible for interaction
        let fileInput = await browser.findElement(By.xpath('//input[@class="dz-hidden-input"]'));
        await browser.executeScript('arguments[0].style.visibility = "visible"; arguments[0].style.height = "100px"; arguments[0].style.width = "100px"', fileInput);

        await fileInput.sendKeys(path.resolve(__dirname, 'test.pdf'));

        // let it be uploaded in the meantime
        await sleep(5000);

        await browser.get(`https://${app.fqdn}/#/documents/documents/1/preview/`);
        await waitForElement(By.xpath('//h3[@id="content-title" and contains(text(), "test.pdf")]'));

        // wait for toast message to hide
        await sleep(5000);
    }

    async function documentExists() {
        await browser.get(`https://${app.fqdn}/#/documents/documents/1/preview/`);

        await waitForElement(By.xpath('//h3[@id="content-title" and contains(text(), "test.pdf")]'));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    // it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can upload document', uploadDocument);
    it('document exists', documentExists);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can login', login);
    it('document exists', documentExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('document exists', documentExists);
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('document exists', documentExists);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // // test update
    // it('can install app', function () { execSync(`cloudron install --appstore-id net.freescout.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    // it('can get app information', getAppInfo);
    // it('can login', login);
    // it('can upload document', uploadDocument);
    // it('document exists', documentExists);
    // it('can logout', logout);

    // it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    // it('can login', login);
    // it('document exists', documentExists);
    // it('can logout', logout);

    // it('uninstall app', function (done) {
    //     // ensure we don't hit NXDOMAIN in the mean time
    //     browser.get('about:blank').then(function () {
    //         execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    //         done();
    //     });
    // });
});
